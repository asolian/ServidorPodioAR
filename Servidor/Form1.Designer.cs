﻿namespace Servidor
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNombreUno = new System.Windows.Forms.TextBox();
            this.lblNombreUno = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nudPromedioUno = new System.Windows.Forms.NumericUpDown();
            this.nudPromedioDos = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tbNombreDos = new System.Windows.Forms.TextBox();
            this.nudPromedioTres = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tbNombreTres = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblLog = new System.Windows.Forms.Label();
            this.btnSwitchServer = new System.Windows.Forms.Button();
            this.rbSexoUnoMujer = new System.Windows.Forms.RadioButton();
            this.rbSexoUnoHombre = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbSexoDosHombre = new System.Windows.Forms.RadioButton();
            this.rbSexoDosMujer = new System.Windows.Forms.RadioButton();
            this.rbSexoTresHombre = new System.Windows.Forms.RadioButton();
            this.rbSexoTresMujer = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioUno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioDos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioTres)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbNombreUno
            // 
            this.tbNombreUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreUno.Location = new System.Drawing.Point(88, 3);
            this.tbNombreUno.Name = "tbNombreUno";
            this.tbNombreUno.Size = new System.Drawing.Size(100, 22);
            this.tbNombreUno.TabIndex = 0;
            this.tbNombreUno.Text = "Mariela";
            this.tbNombreUno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbNombreUno.TextChanged += new System.EventHandler(this.tbNombreUno_TextChanged);
            // 
            // lblNombreUno
            // 
            this.lblNombreUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombreUno.Location = new System.Drawing.Point(3, 4);
            this.lblNombreUno.Name = "lblNombreUno";
            this.lblNombreUno.Size = new System.Drawing.Size(79, 17);
            this.lblNombreUno.TabIndex = 1;
            this.lblNombreUno.Text = "Nombre ";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Promedio";
            // 
            // nudPromedioUno
            // 
            this.nudPromedioUno.DecimalPlaces = 1;
            this.nudPromedioUno.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPromedioUno.Location = new System.Drawing.Point(88, 32);
            this.nudPromedioUno.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPromedioUno.Name = "nudPromedioUno";
            this.nudPromedioUno.Size = new System.Drawing.Size(100, 22);
            this.nudPromedioUno.TabIndex = 3;
            this.nudPromedioUno.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPromedioUno.Value = new decimal(new int[] {
            78,
            0,
            0,
            65536});
            // 
            // nudPromedioDos
            // 
            this.nudPromedioDos.DecimalPlaces = 1;
            this.nudPromedioDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPromedioDos.Location = new System.Drawing.Point(88, 32);
            this.nudPromedioDos.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPromedioDos.Name = "nudPromedioDos";
            this.nudPromedioDos.Size = new System.Drawing.Size(100, 22);
            this.nudPromedioDos.TabIndex = 7;
            this.nudPromedioDos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPromedioDos.Value = new decimal(new int[] {
            34,
            0,
            0,
            65536});
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Promedio";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Nombre ";
            // 
            // tbNombreDos
            // 
            this.tbNombreDos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreDos.Location = new System.Drawing.Point(88, 3);
            this.tbNombreDos.Name = "tbNombreDos";
            this.tbNombreDos.Size = new System.Drawing.Size(100, 22);
            this.tbNombreDos.TabIndex = 4;
            this.tbNombreDos.Text = "Franco";
            this.tbNombreDos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // nudPromedioTres
            // 
            this.nudPromedioTres.DecimalPlaces = 1;
            this.nudPromedioTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPromedioTres.Location = new System.Drawing.Point(88, 32);
            this.nudPromedioTres.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudPromedioTres.Name = "nudPromedioTres";
            this.nudPromedioTres.Size = new System.Drawing.Size(100, 22);
            this.nudPromedioTres.TabIndex = 11;
            this.nudPromedioTres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nudPromedioTres.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Promedio";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Nombre ";
            // 
            // tbNombreTres
            // 
            this.tbNombreTres.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombreTres.Location = new System.Drawing.Point(88, 3);
            this.tbNombreTres.Name = "tbNombreTres";
            this.tbNombreTres.Size = new System.Drawing.Size(100, 22);
            this.tbNombreTres.TabIndex = 8;
            this.tbNombreTres.Text = "Josefina";
            this.tbNombreTres.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel3);
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 242);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Alumnos";
            // 
            // btnEnviar
            // 
            this.btnEnviar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnviar.Location = new System.Drawing.Point(111, 260);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(195, 37);
            this.btnEnviar.TabIndex = 13;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblLog);
            this.groupBox2.Location = new System.Drawing.Point(12, 303);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(294, 64);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Log";
            // 
            // lblLog
            // 
            this.lblLog.Location = new System.Drawing.Point(6, 16);
            this.lblLog.Name = "lblLog";
            this.lblLog.Size = new System.Drawing.Size(282, 45);
            this.lblLog.TabIndex = 0;
            // 
            // btnSwitchServer
            // 
            this.btnSwitchServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSwitchServer.Location = new System.Drawing.Point(12, 260);
            this.btnSwitchServer.Name = "btnSwitchServer";
            this.btnSwitchServer.Size = new System.Drawing.Size(93, 37);
            this.btnSwitchServer.TabIndex = 15;
            this.btnSwitchServer.Text = "ON";
            this.btnSwitchServer.UseVisualStyleBackColor = true;
            this.btnSwitchServer.Click += new System.EventHandler(this.btnSwitchServer_Click);
            // 
            // rbSexoUnoMujer
            // 
            this.rbSexoUnoMujer.AutoSize = true;
            this.rbSexoUnoMujer.Checked = true;
            this.rbSexoUnoMujer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoUnoMujer.Location = new System.Drawing.Point(194, 5);
            this.rbSexoUnoMujer.Name = "rbSexoUnoMujer";
            this.rbSexoUnoMujer.Size = new System.Drawing.Size(59, 20);
            this.rbSexoUnoMujer.TabIndex = 12;
            this.rbSexoUnoMujer.TabStop = true;
            this.rbSexoUnoMujer.Text = "Mujer";
            this.rbSexoUnoMujer.UseVisualStyleBackColor = true;
            // 
            // rbSexoUnoHombre
            // 
            this.rbSexoUnoHombre.AutoSize = true;
            this.rbSexoUnoHombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoUnoHombre.Location = new System.Drawing.Point(194, 31);
            this.rbSexoUnoHombre.Name = "rbSexoUnoHombre";
            this.rbSexoUnoHombre.Size = new System.Drawing.Size(75, 20);
            this.rbSexoUnoHombre.TabIndex = 13;
            this.rbSexoUnoHombre.Text = "Hombre";
            this.rbSexoUnoHombre.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblNombreUno);
            this.panel1.Controls.Add(this.rbSexoUnoHombre);
            this.panel1.Controls.Add(this.nudPromedioUno);
            this.panel1.Controls.Add(this.rbSexoUnoMujer);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tbNombreUno);
            this.panel1.Location = new System.Drawing.Point(9, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(274, 65);
            this.panel1.TabIndex = 14;
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.rbSexoDosHombre);
            this.panel2.Controls.Add(this.rbSexoDosMujer);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.nudPromedioDos);
            this.panel2.Controls.Add(this.tbNombreDos);
            this.panel2.Location = new System.Drawing.Point(9, 92);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(274, 67);
            this.panel2.TabIndex = 15;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.rbSexoTresHombre);
            this.panel3.Controls.Add(this.rbSexoTresMujer);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.nudPromedioTres);
            this.panel3.Controls.Add(this.tbNombreTres);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Location = new System.Drawing.Point(9, 165);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(274, 67);
            this.panel3.TabIndex = 16;
            // 
            // rbSexoDosHombre
            // 
            this.rbSexoDosHombre.AutoSize = true;
            this.rbSexoDosHombre.Checked = true;
            this.rbSexoDosHombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoDosHombre.Location = new System.Drawing.Point(195, 31);
            this.rbSexoDosHombre.Name = "rbSexoDosHombre";
            this.rbSexoDosHombre.Size = new System.Drawing.Size(75, 20);
            this.rbSexoDosHombre.TabIndex = 15;
            this.rbSexoDosHombre.TabStop = true;
            this.rbSexoDosHombre.Text = "Hombre";
            this.rbSexoDosHombre.UseVisualStyleBackColor = true;
            // 
            // rbSexoDosMujer
            // 
            this.rbSexoDosMujer.AutoSize = true;
            this.rbSexoDosMujer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoDosMujer.Location = new System.Drawing.Point(195, 5);
            this.rbSexoDosMujer.Name = "rbSexoDosMujer";
            this.rbSexoDosMujer.Size = new System.Drawing.Size(59, 20);
            this.rbSexoDosMujer.TabIndex = 14;
            this.rbSexoDosMujer.Text = "Mujer";
            this.rbSexoDosMujer.UseVisualStyleBackColor = true;
            // 
            // rbSexoTresHombre
            // 
            this.rbSexoTresHombre.AutoSize = true;
            this.rbSexoTresHombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoTresHombre.Location = new System.Drawing.Point(195, 29);
            this.rbSexoTresHombre.Name = "rbSexoTresHombre";
            this.rbSexoTresHombre.Size = new System.Drawing.Size(75, 20);
            this.rbSexoTresHombre.TabIndex = 15;
            this.rbSexoTresHombre.Text = "Hombre";
            this.rbSexoTresHombre.UseVisualStyleBackColor = true;
            // 
            // rbSexoTresMujer
            // 
            this.rbSexoTresMujer.AutoSize = true;
            this.rbSexoTresMujer.Checked = true;
            this.rbSexoTresMujer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSexoTresMujer.Location = new System.Drawing.Point(195, 3);
            this.rbSexoTresMujer.Name = "rbSexoTresMujer";
            this.rbSexoTresMujer.Size = new System.Drawing.Size(59, 20);
            this.rbSexoTresMujer.TabIndex = 14;
            this.rbSexoTresMujer.TabStop = true;
            this.rbSexoTresMujer.Text = "Mujer";
            this.rbSexoTresMujer.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(321, 373);
            this.Controls.Add(this.btnSwitchServer);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Servidor AR Podio";
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioUno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioDos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPromedioTres)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbNombreUno;
        private System.Windows.Forms.Label lblNombreUno;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudPromedioUno;
        private System.Windows.Forms.NumericUpDown nudPromedioDos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbNombreDos;
        private System.Windows.Forms.NumericUpDown nudPromedioTres;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbNombreTres;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label lblLog;
        private System.Windows.Forms.Button btnSwitchServer;
        private System.Windows.Forms.RadioButton rbSexoUnoHombre;
        private System.Windows.Forms.RadioButton rbSexoUnoMujer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton rbSexoTresHombre;
        private System.Windows.Forms.RadioButton rbSexoTresMujer;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton rbSexoDosHombre;
        private System.Windows.Forms.RadioButton rbSexoDosMujer;
    }
}

