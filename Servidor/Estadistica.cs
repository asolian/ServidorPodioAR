﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Servidor
{
    public class Estadistica
    {
        public String NombreUno { get; set; }
        public Double PromedioUno { get; set; }
        public bool SexoUno { get; set; }

        public String NombreDos { get; set; }
        public Double PromedioDos { get; set; }
        public bool SexoDos { get; set; }

        public String NombreTres { get; set; }
        public Double PromedioTres { get; set; }
        public bool SexoTres { get; set; }

        public bool EsNuevo { get; set; }


        public String Serializar()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
