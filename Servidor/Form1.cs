﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace Servidor
{
    public partial class frmMain : Form
    {
        delegate void logCallBack(Label label, String log);
        Server servidor = new Server();

        public frmMain()
        {
            InitializeComponent();
            servidor.NewLogEvent += Servidor_NewLogEvent;
        }

        private void Servidor_NewLogEvent(string text)
        {
            this.newLog(text);
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(tbNombreUno.Text) ||
                String.IsNullOrEmpty(tbNombreDos.Text) ||
                String.IsNullOrEmpty(tbNombreTres.Text))
            {
                newLog("Debe completar todos los campos.");
                return;
            }
            EnviaDatos(new Estadistica()
            {
                NombreUno = tbNombreUno.Text,
                NombreDos = tbNombreDos.Text,
                NombreTres = tbNombreTres.Text,
                PromedioUno = (Double)nudPromedioUno.Value,
                PromedioDos = (Double)nudPromedioDos.Value,
                PromedioTres = (Double)nudPromedioTres.Value,
                SexoUno = rbSexoUnoHombre.Checked,
                SexoDos = rbSexoDosHombre.Checked,
                SexoTres = rbSexoTresHombre.Checked,
                EsNuevo = true
            });
            
            //EnviaDatos(new Estadistica() { NombreUno = "Pepe", PromedioUno = 3.5, NombreDos = "Tito", PromedioDos = 10, NombreTres = "Maria", PromedioTres = 6.5, EsNuevo = true });
        }

        public void EnviaDatos(Estadistica estadistica)
        {
            try
            {
                servidor.EnviaDatos(estadistica);
            }
            catch (Exception e)
            {
                newLog(e.Message);
            }
        }

        private void btnSwitchServer_Click(object sender, EventArgs e)
        {
            if(btnSwitchServer.Text.Equals("ON"))
            {
                servidor.Iniciar();
                btnSwitchServer.Text = "OFF";
            }
            else
            {
                servidor.Detener();
                btnSwitchServer.Text = "ON";
            }
        }

        public void mostrarLog(Label lbl, String text) {
            if (lbl.InvokeRequired)
            {
                logCallBack delegado = new logCallBack(mostrarLog);
                lbl.Invoke(delegado, new object[] { lbl, text });
            }
            else
            {
                lblLog.Text = text;
            }
        }

        //Atiende la llamada de logs del servidor
        public void newLog(String text)
        {
            mostrarLog(lblLog, text);
            Console.WriteLine(text);
        }

         private void tbNombreUno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
