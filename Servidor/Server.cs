﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace Servidor
{
    public class Server
    {
        bool detener = false;
        TcpListener servidor;
        TcpClient clienteRemoto;
        NetworkStream netStream;
        StreamWriter writer;
        Thread serverThread;
        AutoResetEvent esperarFin = new AutoResetEvent(false);
        AutoResetEvent proximaConexion = new AutoResetEvent(false);

        public delegate void NewLogEventHandler(string text);
        public event NewLogEventHandler NewLogEvent;

        public Server()
        {
            servidor = new TcpListener(IPAddress.Parse("127.0.0.1"), 8888);
            clienteRemoto = new TcpClient();
        }

        public Server(string ipAddress, int port)
        {
            servidor = new TcpListener(IPAddress.Parse(ipAddress), port);
            clienteRemoto = new TcpClient();
        }
        public void Detener()
        {
            NewLogEvent("Detener servidor");
            detener = true;
            
            servidor.Stop();
            NewLogEvent("Esperando finalización...");
            //esperarFin.WaitOne();
        }
        public void Iniciar()
        {
            serverThread = new Thread(new ThreadStart(EsperarConexiones));
            serverThread.Start();
        }

        private void EsperarConexiones()
        {
            detener = false;
            try
            {
                servidor.Start();
            }
            catch (Exception e)
            {
                NewLogEvent(e.Message);
                throw new Exception("Error al iniciar servidor: " + e.Message);
            }

            try
            {
                while (!detener)
                {
                    //Esperando conexiones
                    NewLogEvent("Esperando clientes...");
                    servidor.BeginAcceptTcpClient(new AsyncCallback(NuevaConexion), null);
                    //Espero que me señalicen continuar escuchando
                    proximaConexion.WaitOne();

                }

            }
            catch (Exception e)
            {
                NewLogEvent(e.Message);
            }
            finally {
                
                if (writer != null) writer.Close();
                if (netStream != null) netStream.Close();
                esperarFin.Set();
            }
        }

        private void NuevaConexion(IAsyncResult ar)
        {
            string direccion="";
            string puerto="";
            try
            {
                if (ar != null)
                    clienteRemoto = servidor.EndAcceptTcpClient(ar);
                else //Listener detendido
                    throw new IOException("Servidor detenido");

                //Determino la dirección de origen
                puerto = ((IPEndPoint)clienteRemoto.Client.RemoteEndPoint).Port.ToString();
                direccion = ((IPEndPoint)clienteRemoto.Client.RemoteEndPoint).Address.ToString();

                NewLogEvent("Cliente conectado: ip:puerto -> " + direccion + ":" + puerto);

                //Obtengo los streams necesarios para la comunicación
                netStream = clienteRemoto.GetStream();
                writer = new StreamWriter(netStream, Encoding.ASCII) { AutoFlush = true };

                //espero por datos de forma asincronica
                netStream.BeginRead(new byte[1], 0, 1, RecibirDatos, netStream);
            }
            catch (Exception ex)
            {
                NewLogEvent(ex.Message );
                proximaConexion.Set();
            }   
        }

        private void RecibirDatos(IAsyncResult ar) {
            if (ar.AsyncState != null) {
                int numberOfBytesRead;
                try
                {
                    numberOfBytesRead = netStream.EndRead(ar);
                }
                catch (Exception ex) {
                    NewLogEvent("Error: " + ex.Message);
                    writer.Close();
                    netStream.Close();
                    clienteRemoto.Close();
                    NewLogEvent("Cliente desconenctado");
                    proximaConexion.Set();
                }
            }
        }

        public void EnviaDatos(Estadistica estadistica)
        {
            try
            {
                writer.WriteLine(estadistica.Serializar());
            }
            catch (Exception e)
            {
                writer.Close();
                netStream.Close();
                clienteRemoto.Close();
                NewLogEvent("Error al enviar datos: " + e.Message);
                NewLogEvent("Cliente desconenctado");
                proximaConexion.Set();
            }
        }
    }

}
